import {default as confirmUserSignup} from './confirm-user-signup'
import {default as getImageUploadUrl} from './get-upload-url'
import {default as tweet} from './tweet'

export const functionsConfig = {
  confirmUserSignup,
  getImageUploadUrl,
  tweet,
}
